import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  template: `
      <app-select></app-select>
      <app-select-native></app-select-native>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'job-tinder';
}
