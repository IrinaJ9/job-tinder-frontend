import { UserTechnologies } from "./UserTechnologies";
export interface User {
    id: number;
    username: string;
    password: string;
    firstname: string;
    roles: string[];

    lastname: string;
    email: string;
    location: string;
    relocation: boolean;
    generalexperience: number;
    education: string;
    educationlevel: number;
    remote: boolean;
    userTechnologies: Array<UserTechnologies>;

}
