export interface UserTechnologies {
    id: number;
    typeOfTechology: string;
}