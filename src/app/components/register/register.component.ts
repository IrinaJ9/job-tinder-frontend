import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "../../services/auth.service";
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public user;
  public error: boolean;
  public message: string;

  constructor(
    private router: Router, private authService: AuthenticationService
  ) {
    this.user = {};
  }


  ngOnInit(): void {
  }

  handleRegister() {
    this.authService.register(this.user.username, this.user.password, this.user.email).subscribe(
      (registered: boolean) => {

        this.router.navigate(["/"]);
      })
  }
}
