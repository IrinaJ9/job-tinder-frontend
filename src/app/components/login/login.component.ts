import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user;
  public wrongUsernameOrPass: boolean;
  // public user.username : ['', Validators.required,Validators.email]

  constructor(
    private router: Router, private authService: AuthenticationService) {
    this.user = {}
    // this.wrongUsernameOrPass = false;
  }

  ngOnInit(): void {
  }

  handleLogin() {
    this.authService.login(this.user.username, this.user.password).subscribe(
      (registered: boolean) => {

        this.router.navigate(["/"]);
      })
  }
}




