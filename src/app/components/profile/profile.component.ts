import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "../../services/auth.service";
import { UserService } from '../../services/user.service';
import { User } from 'src/app/models/User';
import { UserTechnologies } from 'src/app/models/UserTechnologies';
import { FormBuilder, Validators } from '@angular/forms';



import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],


})
export class ProfileComponent implements OnInit {

  public user;
  private users: User[];
  private flag: boolean
  // private usertechnologies: UserTechnologies[];
  // technologies = new FormControl();
  // userTechnologies: string[] = ['Java', 'c#', 'Angular', 'React', 'Vue', 'Node'];

  allTechnologies: UserTechnologies[];
  constructor(private router: Router, private formBuilder: FormBuilder, private userService: UserService, private authService: AuthenticationService
  ) {
    this.user = {};
    this.allTechnologies = [];

  }


  ngOnInit(): void {


    // let temp = this.authService.getCurrentUser();
    // console.log(temp);

    // this.user.username = temp.name
    this.allTechnologies = this.userService.getTechnologies();
    this.userService.getAllTechnologies().subscribe(data => { this.allTechnologies = data; });

  }


  // getAllFactories() {
  //   this.flag = false
  //   this.userService.getAllTechnologies.subscribe((data) =>
  //     this.allTechnologies = data
  //   );
  // }


  empForm = this.formBuilder.group({

    technologies: [null, Validators.required]
  });
  get technologies() {
    return this.empForm.get('technologies');
  }

  onFormSubmit() {
    this.userService.saveUser(this.empForm.value);
    this.resetForm();
  }
  resetForm() {
    this.empForm.reset();
  }

  save() {



    this.userService.save(this.user).subscribe(() => this.router.navigate(["/"])
    )

  }



}
