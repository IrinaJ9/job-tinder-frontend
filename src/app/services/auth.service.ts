import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";


@Injectable()
export class AuthenticationService {

  private readonly path = "http://localhost:8080/";
  constructor(private http: HttpClient, private jwtUtilsService: JwtUtilsService) { }

  login(username: string, password: string): Observable<boolean> {
    const headers: HttpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
    return this.http.post(this.path + "login", JSON.stringify({ username, password }), { headers })
      .pipe(map(res => {
        const token = res && res["token"];

        if (token) {
          localStorage.setItem("currentUser", JSON.stringify({
            username: username,
            roles: this.jwtUtilsService.getRoles(token),
            token: token
          }));
          return true;
        } else {
          return false;
        }
      }))
  }

  register(username: string, password: string, email: string): Observable<any> {
    const headers: HttpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
    return this.http.post(this.path + "register", JSON.stringify({ username, password, email }), { headers });
    // .pipe(map(res => {
    //   const token = res && res["token"];
    //   if (token) {
    //     localStorage.setItem("currentUser", JSON.stringify({
    //       username: username,
    //       roles: this.jwtUtilsService.getRoles(token),
    //       token: token
    //     }));
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }))
  }

  getToken(): String {
    const currentUser = JSON.parse(localStorage.getItem("currentUser"));
    const token = currentUser && currentUser.token;
    return token ? token : "";
  }

  logout(): void {
    localStorage.removeItem("currentUser");
  }

  isLoggedIn(): boolean {
    if (this.getToken() !== "") {
      return true;
    } else {
      return false;
    }
  }

  getCurrentUser() {
    if (localStorage.getItem("currentUser")) {
      return JSON.parse(localStorage.getItem("currentUser"));
    } else {
      return undefined;
    }
  }

  getUsers(): Observable<any> {
    return this.http.get<any[]>(this.path + "currentUser");
  }

  isAdmin() {
    const res = this.getCurrentUser();
    if (res) {
      return res.roles.indexOf("Admin") >= 0;
    }
    return false;
  }
}
