import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from '../models/User';
import { UserTechnologies } from '../models/UserTechnologies';

const TECHNOLOGIES = [
  { id: 1, typeOfTechology: 'Java' },
  { id: 2, typeOfTechology: 'Angular' },
  { id: 3, typeOfTechology: 'React' },
  { id: 4, typeOfTechology: 'Vue' },
  { id: 5, typeOfTechology: 'c#' }
];


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly path = "http://localhost:8080/jobseeker";
  constructor(private http: HttpClient) { }

  save(user: User): Observable<User> {
    return this.http.put(this.path + user.id, user) as Observable<User>;
  }

  getAllTechnologies(): Observable<UserTechnologies[]> {
    return this.http.get<UserTechnologies[]>(this.path);
  }


  getTechnologies() {
    return TECHNOLOGIES;
  }
  saveUser(user: User) {
    console.log(user);
  }

  get(id: number): Observable<User> {
    return this.http.get<User>(this.path + id);
  }


}
